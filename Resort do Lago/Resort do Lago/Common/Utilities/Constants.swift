//
//  Constants.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    struct API {
    static let baseURL = "http://app.resortdolago.com.br/api/"
    static let imageBaseURL = "http://app.resortdolago.com.br/thumbnail/medium/"
    }
    
    static let loggedEmail = "LoggedUserEmail"
    static let loggedPassword = "LoggedUserPassword"
    static let loggedUserDetails = "LoggedUserDetails"

}


