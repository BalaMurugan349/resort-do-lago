//
//  Font.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

enum FontWeight: String {
    case regular
    case normal
}

enum FontName: String {
    case varelaRound = "varela Round"
}

struct Font {
    static func make(_ name: FontName, weight: FontWeight, size: CGFloat) -> UIFont {
        let fontName = weight.rawValue == "normal" ? name.rawValue : "\(name.rawValue)-\(weight.rawValue.capitalized)"
        if let font = UIFont(name: fontName, size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }
}
