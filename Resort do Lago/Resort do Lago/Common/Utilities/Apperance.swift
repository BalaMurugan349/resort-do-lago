//
//  Apperance.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 1/11/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

struct Appearance {
    
    // MARK: Views
    static func registrationNavigationController() -> UINavigationController {
        
        let navigationController = UINavigationController()
        
        navigationController.navigationBar.isTranslucent = true
        navigationController.navigationBar.isOpaque = true
        navigationController.navigationBar.barStyle = .default
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.barTintColor = UIColor.buttonBlueBackground
        navigationController.navigationBar.tintColor = UIColor.white
        let titleDict: NSDictionary = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController.navigationBar.titleTextAttributes = titleDict as? [NSAttributedStringKey : Any]
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        
        return navigationController
    }
    
    static func homeNavigationController() -> MainNavigationController {
        
        let navigationController = MainNavigationController()
        
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.isOpaque = true
        navigationController.navigationBar.barStyle = .default
        navigationController.navigationBar.barTintColor = UIColor.navigationBackground
        navigationController.navigationBar.tintColor = UIColor.white
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        let titleDict: NSDictionary = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navigationController.navigationBar.titleTextAttributes = titleDict as? [NSAttributedStringKey : Any]
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)

        return navigationController
    }

}

