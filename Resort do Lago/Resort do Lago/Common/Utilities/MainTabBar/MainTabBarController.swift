//
//  MainTabBarController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 1/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.setNeedsStatusBarAppearanceUpdate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func configureTabBar (){
        var screenSize : String = ""
        if(UIDevice().screenWidth == 320){
            screenSize = ""
        }else if (UIDevice().screenWidth  == 375)
        {
            screenSize = "6"
        }else{
            screenSize = "6+"
        }
        let tabBarBackground : UIImage = UIImage()
        UITabBar.appearance().shadowImage = tabBarBackground
        UITabBar.appearance().backgroundImage = tabBarBackground

        // Create Tab one
        let vc1 = HomeModule.build()
        let tab1 = self.bindinNavigationController(controller: vc1)
        let tabBarItem1 = UITabBarItem(title: nil, image: UIImage(named: "1Unselected\(screenSize)"), selectedImage: UIImage(named: "1Selected\(screenSize)"))
        tab1.tabBarItem = tabBarItem1
        tabBarItem1.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)

        // Create Tab two
        let vc2 = GiftModule.build()
        let tab2 = self.bindinNavigationController(controller: vc2)
        let tabBarItem2 = UITabBarItem(title: nil, image: UIImage(named: "2Unselected\(screenSize)"), selectedImage: UIImage(named: "2Selected\(screenSize)"))
        tab2.tabBarItem = tabBarItem2
        tabBarItem2.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)

        // Create Tab three
        let vc3 = HotelBookingModule.build()
        vc3.view.backgroundColor = UIColor.white
        let tab3 = self.bindinNavigationController(controller: vc3)
        let tabBarItem3 = UITabBarItem(title: nil, image: UIImage(named: "3Unselected\(screenSize)"), selectedImage: UIImage(named: "3Selected\(screenSize)"))
        tab3.tabBarItem = tabBarItem3
        tabBarItem3.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)

        // Create Tab Four
        let vc4 = MarketingModule.build()
        vc4.view.backgroundColor = UIColor.white
        let tab4 = self.bindinNavigationController(controller: vc4)
        let tabBarItem4 = UITabBarItem(title: nil, image: UIImage(named: "4Unselected\(screenSize)"), selectedImage: UIImage(named: "4Selected\(screenSize)"))
        tab4.tabBarItem = tabBarItem4
        tabBarItem4.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)

        self.viewControllers = [tab1, tab2, tab3, tab4]

    }
    
    func bindinNavigationController (controller : UIViewController) -> MainNavigationController{
        let navVC = Appearance.homeNavigationController()
        navVC.viewControllers = [controller]
        return navVC
    }

    override var preferredStatusBarStyle: UIStatusBarStyle{
        UIApplication.shared.isStatusBarHidden = true
        UIApplication.shared.isStatusBarHidden = false
        return UIStatusBarStyle.lightContent
    }

    override var prefersStatusBarHidden: Bool{
        return false
    }

}
