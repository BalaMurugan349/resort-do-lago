//
//  HomeCollectionViewCell.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/20/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class GalleryCollectionViewCell: UICollectionViewCell {
    let imageViewThumbnail : UIImageView = UIImageView()
    
    func setupCell (galleryData : Gallery){
        self.backgroundColor = UIColor.white
        if let url = URL(string: galleryData.galleryImage){
            imageViewThumbnail.kf.setImage(with: url)
        }
        self.addSubview(imageViewThumbnail)
        constrain(imageViewThumbnail,self) { (imageViewThumbnail,view) in
            imageViewThumbnail.edges == view.edges
        }
    }
}
