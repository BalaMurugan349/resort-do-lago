//
//  Enterprises.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/24/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import ObjectMapper

struct Enterprises : Mappable {
    var video : FeaturedVideo = FeaturedVideo()
    var companies : [Company] = []
    
    init() {}
    init?(map: Map){}
    mutating func mapping(map: Map) {
        video <- map["data.featured_video"]
        companies <- map["data.enterprises.data"]
    }

}

struct FeaturedVideo : Mappable {
    var videoId : String = ""
    var videoType : String = ""
    var videoUrl : String = ""
    
    init() {}
    init?(map: Map){}
    mutating func mapping(map: Map) {
        videoId <- map["id"]
        videoType <- map["type"]
        videoUrl <- map["video_url"]
    }

}

struct Company : Mappable {
    var companyId : String = ""
    var companyName : String = ""
    var companyImageName : String = ""
    var companyDescription : String = ""
    var companyFeaturedImage : String = ""
    var companyMapImage : String = ""
    var companyAbout : String = ""
    var companyGallery : [Gallery] = []
    
    init() {}
    init?(map: Map){}
    mutating func mapping(map: Map) {
        companyId <- map["id"]
        companyName <- map["name"]
        companyImageName <- map["image_name"]
        companyDescription <- map["description"]
        companyFeaturedImage <- map["featured_image"]
        companyMapImage <- map["map_image"]
        companyAbout <- map["about"]
        companyGallery <- map["gallery"]
    }

}

struct Gallery : Mappable {
    var galleryId : String = ""
    var galleryImage : String = ""
    
    init() {}
    init?(map: Map){}
    mutating func mapping(map: Map) {
        galleryId <- map["id"]
        galleryImage <- map["image_url"]

    }

}
