//
//  LoggedUser.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/7/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

//CHECK NIL VALUE
func CheckForNull(strToCheck : String?) -> String{
    if let strText = strToCheck {
        return strText
    }
    return "nil"
}

let localeSelected = Localization.sharedInstance.getLanguageISO() == "pt" ? "1" : "0"

struct LoggedUser {
    
    static var shared : LoggedUser = LoggedUser()
    var accountStatus : String = ""
    var cpf : String = ""
    var createdDate : String = ""
    var dob : String = ""
    var email : String = ""
    var fullName : String = ""
    var gender : String = ""
    var userid : String = ""
    var is_Alien : Bool = false
    var resortId : String = ""
    var token : String = ""
    
    
    mutating func storeLoggedUserDetails (dict : NSDictionary){
        accountStatus = CheckForNull(strToCheck: dict.checkValueForKey(key: "account_status") as? String)
        cpf = CheckForNull(strToCheck: dict.checkValueForKey(key: "cpf") as? String)
        createdDate = CheckForNull(strToCheck: dict.checkValueForKey(key: "created_at") as? String)
        dob = CheckForNull(strToCheck: dict.checkValueForKey(key: "dob") as? String)
        email = CheckForNull(strToCheck: dict.checkValueForKey(key: "email") as? String)
        fullName = CheckForNull(strToCheck: dict.checkValueForKey(key: "full_name") as? String)
        gender = CheckForNull(strToCheck: dict.checkValueForKey(key: "gender") as? String)
        userid = CheckForNull(strToCheck: dict.checkValueForKey(key: "id") as? String)
        is_Alien = dict["isAlien"] as! Bool
        resortId = CheckForNull(strToCheck: dict.checkValueForKey(key: "resort_id") as? String)
        token = CheckForNull(strToCheck: dict.checkValueForKey(key: "token") as? String)

    }
    
    static func saveLoggedUserdetails(dictDetails : NSDictionary){
        let data : NSData = NSKeyedArchiver.archivedData(withRootObject: dictDetails) as NSData
        UserDefaults.standard.set(data, forKey: Constants.loggedUserDetails)
        UserDefaults.standard.synchronize()
    }

}
