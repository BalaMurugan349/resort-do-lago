//
//  Color.swift
//  Resort do Lago
//
//  Created by P, Balamurugan(AWF) on 27/09/17.
//  Copyright © 2017 P, Balamurugan(AWF). All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    public class var buttonBlueBackground: UIColor { return UIColor(red: 63.0/255.0, green: 162.0/255.0, blue: 239.0/255.0, alpha: 0.4) }
    public class var buttonPinkBackground: UIColor { return UIColor(red: 181.0/255.0, green: 146.0/255.0, blue: 220.0/255.0, alpha: 0.7) }
    public class var navigationBackground: UIColor { return UIColor(red: 78.0/255.0, green: 117.0/255.0, blue: 121.0/255.0, alpha: 1.0) }
    public class var buttonGreenBackground: UIColor { return UIColor(red: 37.0/255.0, green: 184.0/255.0, blue: 36.0/255.0, alpha: 1.0) }
    public class var buttonDarkPinkBackground: UIColor { return UIColor(red: 235.0/255.0, green: 74.0/255.0, blue: 148.0/255.0, alpha: 1.0) }
    public class var labelBackground: UIColor { return UIColor(red: 234.0/255.0, green: 243.0/255.0, blue: 243.0/255.0, alpha: 1.0) }
    public class var cellBackground: UIColor { return UIColor(red: 201.0/255.0, green: 224.0/255.0, blue: 228.0/255.0, alpha: 1.0) }
    public class var topTabBackground: UIColor { return UIColor(red: 241.0/255.0, green: 148.0/255.0, blue: 50.0/255.0, alpha: 1.0) }







}
