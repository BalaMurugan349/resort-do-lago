//
//  UIViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 1/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func showAlert(with message: String) {
        let alertController = UIAlertController(title: "Resort do Lago", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .destructive) { (action) in
        }
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }

}
