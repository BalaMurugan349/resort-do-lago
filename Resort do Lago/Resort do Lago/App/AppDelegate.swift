//
//  AppDelegate.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/15/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import CoreData
import MFSideMenu

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    @objc var videoIsInFullscreen: NSNumber = 0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.checkAutoLogin()
        return true
    }
    
    //AUTO LOGIN
    func checkAutoLogin (){
        if (UserDefaults.standard.object(forKey: Constants.loggedUserDetails) != nil)
        {
            let userDetails : NSDictionary = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: Constants.loggedUserDetails) as! Data) as! [String : Any] as NSDictionary
            LoggedUser.shared.storeLoggedUserDetails(dict: userDetails)
            didLogin()
        }else{
            gotoWelcomePage()
        }
    }
    
    //WELCOME PAGE
    func gotoWelcomePage (){
        let welcomeVC = WelcomeModule.build()
        let navVC = Appearance.registrationNavigationController()
        navVC.isNavigationBarHidden = true
        navVC.viewControllers = [welcomeVC]
        self.window?.rootViewController = navVC
    }

    //HOME PAGE
    func didLogin()  {
        let tabVC = MainTabBarController()
        tabVC.configureTabBar()
        let sideMenu = SideMenuTableViewController(style: UITableViewStyle.plain)
        let sideNavigation = Appearance.homeNavigationController()
        sideNavigation.viewControllers = [sideMenu]
        sideNavigation.isNavigationBarHidden = true
        let container : MFSideMenuContainerViewController = MFSideMenuContainerViewController.container(withCenter: tabVC, leftMenuViewController: sideNavigation, rightMenuViewController: nil)
        container.leftMenuWidth = 250
        container.panMode = MFSideMenuPanModeNone
        self.window?.rootViewController = container

    }
    
//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        if(videoIsInFullscreen == 1) {
//            return UIInterfaceOrientationMask.allButUpsideDown;
//        }
//        else {
//            return UIInterfaceOrientationMask.portrait;
//        }
//    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
//        self.saveContext()
    }

//    // MARK: - Core Data stack
//
//    lazy var persistentContainer: NSPersistentContainer = {
//        /*
//         The persistent container for the application. This implementation
//         creates and returns a container, having loaded the store for the
//         application to it. This property is optional since there are legitimate
//         error conditions that could cause the creation of the store to fail.
//        */
//        let container = NSPersistentContainer(name: "Resort_do_Lago")
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//
//                /*
//                 Typical reasons for an error here include:
//                 * The parent directory does not exist, cannot be created, or disallows writing.
//                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
//                 * The device is out of space.
//                 * The store could not be migrated to the current model version.
//                 Check the error message to determine what the actual problem was.
//                 */
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//
//    // MARK: - Core Data Saving support
//
//    func saveContext () {
//        let context = persistentContainer.viewContext
//        if context.hasChanges {
//            do {
//                try context.save()
//            } catch {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        }
//    }

}

