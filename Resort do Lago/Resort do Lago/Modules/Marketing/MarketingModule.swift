//
//  MarketingModule.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

struct MarketingModule {
    
    static func build() -> MarketingViewController {
        let wireframe = MarketingWireframe()
        let viewModel = MarketingViewModel(wireframe: wireframe)
        let viewController = MarketingViewController(viewModel: viewModel)
        viewModel.view = viewController as MarketingViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }
}
