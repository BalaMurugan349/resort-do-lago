//
//  MarketingInterfaces.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

protocol MarketingViewModelOutput: class {
    func showAlert(with message: String)
}

protocol MarketingViewModelInput: class {
    
}
