//
//  MarketingViewModel.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class MarketingViewModel: MarketingViewModelInput {
    
    private let wireframe: MarketingWireframe
    weak var view: MarketingViewModelOutput!
    
    init(wireframe: MarketingWireframe) {
        self.wireframe = wireframe
    }
}
