//
//  MarketingViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class MarketingViewController: UIViewController,MarketingViewModelOutput {
    
    private let viewModel: MarketingViewModelInput
    internal var webViewMarketing : UIWebView = UIWebView()

    init(viewModel: MarketingViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.title = "Grupo Resort do Lago"
        self.setupLabelTitle()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLabelTitle (){
        //Label Title
        let labelTitle : UILabel = UILabel()
        self.view.addSubview(labelTitle)
        labelTitle.font = Font.make(FontName.varelaRound, weight: FontWeight.normal
            , size: 15.0)
        labelTitle.textColor = UIColor.navigationBackground
        labelTitle.backgroundColor = UIColor.labelBackground
        labelTitle.textAlignment = .center
        labelTitle.text = "Marketing multi-nível"
        constrain(labelTitle,self.view) { (labelTitle,view) in
            labelTitle.leading == view.leading
            labelTitle.trailing == view.trailing
            labelTitle.top == view.top
            labelTitle.height == 30
        }
        
        self.view.addSubview(webViewMarketing)
        webViewMarketing.backgroundColor = UIColor.white
        webViewMarketing.loadRequest(URLRequest.init(url: URL(string: "http://reservas.resortdolago.com.br")!))
        constrain(webViewMarketing,self.view,labelTitle) { (webViewMarketing, view, labelTitle) in
            webViewMarketing.top == labelTitle.bottom
            webViewMarketing.leading == view.leading
            webViewMarketing.trailing == view.trailing
            webViewMarketing.bottom == view.bottom
        }

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    override var prefersStatusBarHidden: Bool{
        return false
    }

}
