//
//  WelcomeViewModel.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class WelcomeViewModel: WelcomeViewModelInput {
    
    private let wireframe: WelcomeWireframe
    weak var view: WelcomeViewModelOutput!
    
    init(wireframe: WelcomeWireframe) {
        self.wireframe = wireframe
    }
    
    func buttonHeight () -> CGFloat {
        switch UIDevice().screenType {
        case .iPhone6Plus,.iPhoneX:
            return 50
        default:
            return 40
        }
    }

    //SUBSCRIBER BUTTON
    func userPressedSubscriberButton (){
        wireframe.gotoLoginPage()
    }
    
    //GUEST BUTTON
    func userPressedGuestButton (){
        wireframe.didLogin()
    }

}
