//
//  WelcomeViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class WelcomeViewController: VideoSplashViewController,WelcomeViewModelOutput {

    private let viewModel: WelcomeViewModelInput
    private let imageViewTempBG = UIImageView()
    
    init(viewModel: WelcomeViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setupView(){
        self.view.backgroundColor = UIColor.white
        self.setupVideo()
    }

    //BACKGROUND VIDEO
    func setupVideo (){
        let url = NSURL.fileURL(withPath: Bundle.main.path(forResource: "beach", ofType: "mp4")!)
        self.videoFrame = view.frame
        self.fillMode = .resizeAspectFill
        self.alwaysRepeat = true
        self.sound = true
        self.backgroundColor = UIColor.black
        self.contentURL = url
        self.restartForeground = true
        self.setupUI()
    }
    
    override func movieReadyToPlay() {
        imageViewTempBG.removeFromSuperview()
    }
    
    func setupUI (){
        let viewBg : UIView = UIView()
        viewBg.backgroundColor = UIColor.clear
        self.view.addSubview(viewBg)
        constrain(viewBg,view) { (view1,view2) in
            view1.edges == view2.edges
        }
        
        //VIEW LINE
        let viewLine : UIView = UIView()
        viewLine.backgroundColor = UIColor.white
        viewBg.addSubview(viewLine)
        constrain(viewLine,viewBg) { (view1,view2) in
            view1.leading == view2.leading + 120
            view1.trailing == view2.trailing - 120
            view1.height == 1
            view1.center == view2.center
        }
        
        //SUBSCRIBER BUTTON
        let buttonSubscriber = UIButton(type: UIButtonType.custom)
        buttonSubscriber.configure(title: "Já sou Associado".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.clear)
        buttonSubscriber.setBackgroundImage(#imageLiteral(resourceName: "BlueBackground"), for: UIControlState.normal)
        buttonSubscriber.addTarget(self, action: #selector(onSubscriberButtonPressed), for: UIControlEvents.touchUpInside)
        viewBg.addSubview(buttonSubscriber)
        constrain(buttonSubscriber,viewBg,viewLine) { (buttonSubscriber,view,viewLine) in
            buttonSubscriber.bottom == viewLine.bottom - 30
            buttonSubscriber.leading == view.leading + 60
            buttonSubscriber.trailing == view.trailing - 60
            buttonSubscriber.height == self.viewModel.buttonHeight()
        }

        //GUEST USER BUTTON
        let buttonGuest = UIButton(type: UIButtonType.custom)
        buttonGuest.configure(title: "Ainda não sou Associado".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.clear)
        buttonGuest.setBackgroundImage(#imageLiteral(resourceName: "PinkBackground"), for: UIControlState.normal)
        buttonGuest.addTarget(self, action: #selector(onGuestButtonPressed), for: UIControlEvents.touchUpInside)
        viewBg.addSubview(buttonGuest)
        constrain(buttonGuest,viewBg,viewLine) { (buttonGuest,view,viewLine) in
            buttonGuest.top == viewLine.top + 30
            buttonGuest.leading == view.leading + 60
            buttonGuest.trailing == view.trailing - 60
            buttonGuest.height == self.viewModel.buttonHeight()
        }
        
        let viewLogo = UIView()
        viewLogo.backgroundColor = UIColor.clear
        viewBg.addSubview(viewLogo)
        constrain(viewLogo,viewBg,buttonSubscriber) { (viewLogo,viewBg,buttonSubscriber) in
            viewLogo.leading == viewBg.leading
            viewLogo.trailing == viewBg.trailing
            viewLogo.top == viewBg.top
            viewLogo.bottom == buttonSubscriber.top
        }

        let imgvwLogo = UIImageView(image: #imageLiteral(resourceName: "ResortLogo"))
        viewLogo.addSubview(imgvwLogo)
        constrain(imgvwLogo,viewLogo) { (imgvwLogo,viewLogo) in
            imgvwLogo.width == 190
            imgvwLogo.height == 125
            imgvwLogo.centerX == viewLogo.centerX
            imgvwLogo.centerY == viewLogo.centerY + 20
        }

        imageViewTempBG.image = UIImage(named: "WelcomeBackground".backgroundImageName())
        viewBg.addSubview(imageViewTempBG)
        constrain(imageViewTempBG,viewBg) { (imageViewTempBG,viewBg) in
            imageViewTempBG.edges == viewBg.edges
        }
    }
    
    //MARK:- BUTTON ACTIONS
    //SUBSCRIBER BUTTON
    @objc func onSubscriberButtonPressed (){
        self.viewModel.userPressedSubscriberButton()
    }
    
    //GUEST BUTTON
    @objc func onGuestButtonPressed (){
        self.viewModel.userPressedGuestButton()
    }
}
