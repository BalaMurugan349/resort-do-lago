//
//  WelcomeInterfaces.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import CoreGraphics

protocol WelcomeViewModelOutput: class {
    
}

protocol WelcomeViewModelInput: class {
    func buttonHeight () -> CGFloat
    func userPressedSubscriberButton ()
    func userPressedGuestButton ()
}
