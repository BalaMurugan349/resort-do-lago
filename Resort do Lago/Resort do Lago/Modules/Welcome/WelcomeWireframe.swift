//
//  WelcomeWireframe.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class WelcomeWireframe {
    
    weak var viewController: WelcomeViewController!
    
    func gotoLoginPage (){
        let loginVC = LoginModule.build()
        viewController.navigationController?.pushViewController(loginVC, animated: true)
    }
    

    //HOME
    func didLogin (){
        (UIApplication.shared.delegate as! AppDelegate).didLogin()
    }

}
