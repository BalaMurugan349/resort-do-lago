//
//  WelcomeModule.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

struct WelcomeModule {
    
    static func build() ->WelcomeViewController {
        let wireframe = WelcomeWireframe()
        let viewModel = WelcomeViewModel(wireframe: wireframe)
        let viewController = WelcomeViewController(viewModel: viewModel)
        viewModel.view = viewController as WelcomeViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }
}
