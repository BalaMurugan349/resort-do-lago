//
//  GuestUserInterfaces.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

protocol GuestUserViewModelOutput: class {
    func showAlert(with message: String)
}

protocol GuestUserViewModelInput: class {
    func loginTableCellHeight () -> CGFloat
    func userPressedSearhButton (cpf : String, dob : String)
    func userPressedYesButton ()
//    func userPressedNoButton ()
    func userPressedGuestLoginButton ()
}
