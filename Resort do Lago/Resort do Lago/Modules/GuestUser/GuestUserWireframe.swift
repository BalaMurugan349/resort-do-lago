//
//  GuestUserWireframe.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class GuestUserWireframe {
    
    weak var viewController: GuestUserTableViewController!
    
    func gotoGuestUserPage1 (){
        let guestVC = GuestUserModule.buildPage1()
        viewController.navigationController?.pushViewController(guestVC, animated: true)
    }
    
    func gotoGuestUserPage2 (){
        let guestVC = GuestUserModule.buildPage2()
        viewController.navigationController?.pushViewController(guestVC, animated: true)
    }
    
    func gotoGuestUserPage3 (){
        let guestVC = GuestUserModule.buildPage3()
        viewController.navigationController?.pushViewController(guestVC, animated: true)
    }


    //HOME
    func didLogin (){
        (UIApplication.shared.delegate as! AppDelegate).didLogin()
    }

}
