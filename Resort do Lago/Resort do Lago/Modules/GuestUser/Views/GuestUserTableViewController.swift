//
//  GuestUserTableViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class GuestUserTableViewController: UITableViewController,GuestUserViewModelOutput {

    let viewModel: GuestUserViewModelInput
    private let tableHeaderHeight : CGFloat = UIDevice().screenHeight * 0.30
    var arrayTableViewCells : [LoginTableViewCell] = []
    private let cpfCell : LoginTableViewCell = LoginTableViewCell()
    private let dobCell : LoginTableViewCell = LoginTableViewCell()

    init(viewModel: GuestUserViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Recuperar Acesso"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setupView() {
        self.navigationItem.hidesBackButton = true
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "BackButton"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(onBackButtonPressed))
        self.navigationItem.leftBarButtonItem = backButton
        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "LoginBackground".backgroundImageName())
        self.tableView.backgroundView = imgvw
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.setupTableHeaderView()
        self.setupTableViewCells()
        self.setUpFooterView()

    }
    
    //TABLE HEADER VIEW
    func setupTableHeaderView (){
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: tableHeaderHeight))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let labelNameText : UILabel = UILabel()
        labelNameText.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        labelNameText.textColor = UIColor.white
        labelNameText.textAlignment = .center
        labelNameText.numberOfLines = 0
        labelNameText.text = "Deixa a gente te ajudar a recuperar seu acesso no aplicaitivo do Resort do Lago? É fácil e não demora nada! \n\n Informe os dados a seguir:".localize
        viewHeader.addSubview(labelNameText)
        constrain(labelNameText,viewHeader) { (labelNameText,viewHeader) in
            labelNameText.leading == viewHeader.leading + 30
            labelNameText.trailing == viewHeader.trailing - 30
            labelNameText.bottom == viewHeader.bottom - 20
            labelNameText.height == 110
        }
        
        let topPadding : CGFloat = UIDevice().screenType == .iPhone5 ? 0 : 10
        let imgLock = UIImageView(image: #imageLiteral(resourceName: "Lock"))
        viewHeader.addSubview(imgLock)
        constrain(imgLock,labelNameText,viewHeader) { (imgLock,labelNameText,viewHeader) in
            imgLock.bottom == labelNameText.top - topPadding
            imgLock.height == 40
            imgLock.width == 40
            imgLock.centerX == viewHeader.centerX
        }

    }
    
    //TABLE VIEW CELLS
    func setupTableViewCells (){
        cpfCell.setupWith(placeholder: "SEU CPF".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: #imageLiteral(resourceName: "CPF"), pickerType: .None)
        arrayTableViewCells.append(cpfCell)
        
        dobCell.setupWith(placeholder: "SUA DATA NASCIMENTO".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: #imageLiteral(resourceName: "DOB"), pickerType: .DateOfBirth)
        arrayTableViewCells.append(dobCell)
        self.tableView.reloadData()
    }
    
    //TABLE FOOTER VIEW
    func setUpFooterView (){
        let height = UIDevice().screenHeight - (self.viewModel.loginTableCellHeight() * 2) - tableHeaderHeight - UIDevice().statusBarHeight
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: height))
        self.tableView.tableFooterView = viewFooter
        
        //VIEW LINE
        let viewLine : UIView = UIView()
        viewLine.backgroundColor = UIColor.white
        viewFooter.addSubview(viewLine)
        constrain(viewLine,viewFooter) { (view1,view2) in
            view1.leading == view2.leading + 120
            view1.trailing == view2.trailing - 120
            view1.height == 1
            view1.top == view2.top + 15
        }
        
        //SEARCH BUTTON
        let buttonSearch = UIButton(type: UIButtonType.custom)
        buttonSearch.configure(title: "PESQUISAR".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.clear)
        buttonSearch.setBackgroundImage(#imageLiteral(resourceName: "BlueBackground"), for: UIControlState.normal)
        buttonSearch.addTarget(self, action: #selector(onSearchButtonPressed), for: UIControlEvents.touchUpInside)
        viewFooter.addSubview(buttonSearch)
        constrain(buttonSearch,viewFooter,viewLine) { (buttonSearch,view,viewLine) in
            buttonSearch.top == viewLine.top + 20
            buttonSearch.leading == view.leading + 80
            buttonSearch.trailing == view.trailing - 80
            buttonSearch.height == self.viewModel.loginTableCellHeight() - 10
        }
        
    }

    //MARK:- UITABLEVIEW DELEGATE
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTableViewCells.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.viewModel.loginTableCellHeight()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return arrayTableViewCells[indexPath.row]
    }

    //MARK:- BUTTON ACTIONS
    //SEARCH BUTTON
    @objc func onSearchButtonPressed (){
        self.viewModel.userPressedSearhButton(cpf: cpfCell.textfieldCustom.text!, dob: dobCell.textfieldCustom.text!)
    }
    
    //BACK BUTTON
    @objc func onBackButtonPressed (){
        self.navigationController?.popViewController(animated: true)
    }

}
