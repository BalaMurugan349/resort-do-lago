//
//  GuestUser1TableViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class GuestUser1TableViewController: GuestUserTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //TABLE HEADER VIEW
    override func setupTableHeaderView (){
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: (UIDevice().screenHeight * 0.50) - 40))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let labelHeading : UILabel = UILabel()
        labelHeading.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 30.0)
        labelHeading.textColor = UIColor.white
        labelHeading.textAlignment = .center
        labelHeading.numberOfLines = 0
        labelHeading.text = LoggedUser.shared.fullName
        viewHeader.addSubview(labelHeading)
        constrain(labelHeading,viewHeader) { (labelHeading,viewHeader) in
            labelHeading.leading == viewHeader.leading + 30
            labelHeading.trailing == viewHeader.trailing - 30
            labelHeading.bottom == viewHeader.bottom - 20
            labelHeading.height == 110
        }
        
        let labelTitle : UILabel = UILabel()
        labelTitle.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        labelTitle.textColor = UIColor.white
        labelTitle.textAlignment = .center
        labelTitle.numberOfLines = 0
        labelTitle.text = "Opa! Parece que achamos seu contrato ;) Esse é seu nome?".localize
        viewHeader.addSubview(labelTitle)
        constrain(labelTitle,labelHeading) { (labelTitle,labelHeading) in
            labelTitle.leading == labelHeading.leading
            labelTitle.trailing == labelHeading.trailing
            labelTitle.bottom == labelHeading.top - 20
            labelTitle.height == 60
        }

        let topPadding : CGFloat = UIDevice().screenType == .iPhone5 ? 0 : 10
        let imgLock = UIImageView(image: #imageLiteral(resourceName: "CpfCircle"))
        viewHeader.addSubview(imgLock)
        constrain(imgLock,labelTitle,viewHeader) { (imgLock,labelTitle,viewHeader) in
            imgLock.bottom == labelTitle.top - topPadding
            imgLock.height == 40
            imgLock.width == 40
            imgLock.centerX == viewHeader.centerX
        }

    }
    
    //TABLE VIEW CELLS
    override func setupTableViewCells (){
    }
    
    //TABLE FOOTER VIEW
    override func setUpFooterView (){
        let height = (UIDevice().screenHeight * 0.50) - UIDevice().navigationBarHeight
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: height))
        self.tableView.tableFooterView = viewFooter
        
        //VIEW LINE
        let viewLine : UIView = UIView()
        viewLine.backgroundColor = UIColor.white
        viewFooter.addSubview(viewLine)
        constrain(viewLine,viewFooter) { (view1,view2) in
            view1.leading == view2.leading + 120
            view1.trailing == view2.trailing - 120
            view1.height == 1
            view1.top == view2.top + 15
        }
        
        //YES BUTTON
        let buttonYES = UIButton(type: UIButtonType.custom)
        buttonYES.configure(title: "SIM, SOU EU".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.clear)
        buttonYES.setBackgroundImage(#imageLiteral(resourceName: "GreenBackground"), for: UIControlState.normal)
        buttonYES.addTarget(self, action: #selector(onYesButtonPressed), for: UIControlEvents.touchUpInside)
        viewFooter.addSubview(buttonYES)
        constrain(buttonYES,viewFooter,viewLine) { (buttonYES,view,viewLine) in
            buttonYES.top == viewLine.top + 20
            buttonYES.leading == view.leading + 80
            buttonYES.trailing == view.trailing - 80
            buttonYES.height == self.viewModel.loginTableCellHeight() - 10
        }
        
        //NO BUTTON
        let buttonNO = UIButton(type: UIButtonType.custom)
        buttonNO.configure(title: "NÃO ;(".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.clear)
        buttonNO.setBackgroundImage(#imageLiteral(resourceName: "PinkBackground"), for: UIControlState.normal)
        buttonNO.addTarget(self, action: #selector(onNoButtonPressed), for: UIControlEvents.touchUpInside)
        viewFooter.addSubview(buttonNO)
        constrain(buttonNO,viewFooter,buttonYES) { (buttonNO,view,buttonYES) in
            buttonNO.top == buttonYES.bottom + 20
            buttonNO.leading == view.leading + 80
            buttonNO.trailing == view.trailing - 80
            buttonNO.height == self.viewModel.loginTableCellHeight() - 10
        }

        
    }
    
    //MARK:- BUTTON ACTIONS
    //YES BUTTON
    @objc func onYesButtonPressed (){
        self.viewModel.userPressedYesButton()
    }
    
    //NO BUTTON
    @objc func onNoButtonPressed (){
       // self.viewModel.userPressedNoButton()
        self.navigationController?.popViewController(animated: true)
    }

}
