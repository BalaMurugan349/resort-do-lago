//
//  GuestUser2TableViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class GuestUser3TableViewController: GuestUserTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TABLE HEADER VIEW
    override func setupTableHeaderView (){
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: (UIDevice().screenHeight * 0.50)))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let labelHeading : UILabel = UILabel()
        labelHeading.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        labelHeading.textColor = UIColor.white
        labelHeading.textAlignment = .center
        labelHeading.numberOfLines = 0
        labelHeading.text = "Acesse sua caixa de mensagens para obter seu NÚMERO CONTRATO e SENHA de acesso ao aplicativo do Resort do Lago.".localize
        viewHeader.addSubview(labelHeading)
        constrain(labelHeading,viewHeader) { (labelHeading,viewHeader) in
            labelHeading.leading == viewHeader.leading + 30
            labelHeading.trailing == viewHeader.trailing - 30
            labelHeading.bottom == viewHeader.bottom - 20
            labelHeading.height == 75
        }
        
        let labelEmail : UILabel = UILabel()
        labelEmail.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 30.0)
        labelEmail.textColor = UIColor.white
        labelEmail.textAlignment = .center
        labelEmail.numberOfLines = 0
        labelEmail.adjustsFontSizeToFitWidth = true
        labelEmail.text = trimmedEmail()//"W...@gmail.com".localize
        viewHeader.addSubview(labelEmail)
        constrain(labelEmail,labelHeading) { (labelEmail,labelHeading) in
            labelEmail.leading == labelHeading.leading
            labelEmail.trailing == labelHeading.trailing
            labelEmail.bottom == labelHeading.top - 20
            labelEmail.height == 50
        }
        
        let labelTitle : UILabel = UILabel()
        labelTitle.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        labelTitle.textColor = UIColor.white
        labelTitle.textAlignment = .center
        labelTitle.numberOfLines = 0
        labelTitle.text = "Obrigado por confirmar sua identidade. Seu Resort ID acaba de ser enviado para seu e-mail:".localize
        viewHeader.addSubview(labelTitle)
        constrain(labelTitle,labelEmail) { (labelTitle,labelEmail) in
            labelTitle.leading == labelEmail.leading
            labelTitle.trailing == labelEmail.trailing
            labelTitle.bottom == labelEmail.top - 20
            labelTitle.height == 70
        }

        let topPadding : CGFloat = UIDevice().screenType == .iPhone5 ? 0 : 10
        let imgLock = UIImageView(image: #imageLiteral(resourceName: "EmailCircle"))
        viewHeader.addSubview(imgLock)
        constrain(imgLock,labelTitle,viewHeader) { (imgLock,labelTitle,viewHeader) in
            imgLock.bottom == labelTitle.top - topPadding
            imgLock.height == 40
            imgLock.width == 40
            imgLock.centerX == viewHeader.centerX
        }

    }
    
    //TRIM EMAIl
    func trimmedEmail () -> String{
        let email = LoggedUser.shared.email
        if email.isValidEmail{
            let emailArray : [String] = email.components(separatedBy: "@")
            let emailTrimmed = "\(email.first!)" + "...@" + emailArray[1]
            return emailTrimmed
        }
        return ""
    }
    
    //TABLE VIEW CELLS
    override func setupTableViewCells (){
    }
    
    //TABLE FOOTER VIEW
    override func setUpFooterView (){
        let height = (UIDevice().screenHeight * 0.50) - UIDevice().navigationBarHeight
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: height))
        self.tableView.tableFooterView = viewFooter
        
        //VIEW LINE
        let viewLine : UIView = UIView()
        viewLine.backgroundColor = UIColor.white
        viewFooter.addSubview(viewLine)
        constrain(viewLine,viewFooter) { (view1,view2) in
            view1.leading == view2.leading + 120
            view1.trailing == view2.trailing - 120
            view1.height == 1
            view1.top == view2.top + 15
        }
        
        //LOGIN BUTTON
        let buttonLogin = UIButton(type: UIButtonType.custom)
        buttonLogin.configure(title: "FAZER LOGIN".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.clear)
        buttonLogin.setBackgroundImage(#imageLiteral(resourceName: "BlueBackground"), for: UIControlState.normal)
        buttonLogin.addTarget(self, action: #selector(onLoginButtonPressed), for: UIControlEvents.touchUpInside)
        viewFooter.addSubview(buttonLogin)
        constrain(buttonLogin,viewFooter,viewLine) { (buttonLogin,view,viewLine) in
            buttonLogin.top == viewLine.top + 30
            buttonLogin.leading == view.leading + 80
            buttonLogin.trailing == view.trailing - 80
            buttonLogin.height == self.viewModel.loginTableCellHeight() - 10
        }
        
    }
    
    //MARK:- BUTTON ACTIONS
    //GUEST LOGIN BUTTON
    @objc func onLoginButtonPressed (){
        self.viewModel.userPressedGuestLoginButton()
    }

    
    
}
