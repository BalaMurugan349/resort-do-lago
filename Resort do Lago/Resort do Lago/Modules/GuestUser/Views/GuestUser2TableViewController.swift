//
//  GuestUser2TableViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/12/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class GuestUser2TableViewController: GuestUserTableViewController {

    private let tableHeaderHeight : CGFloat = UIDevice().screenHeight * 0.40
    private let emailCell : LoginTableViewCell = LoginTableViewCell()

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func setupView() {
        self.navigationItem.hidesBackButton = true
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "BackButton"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(onBackButtonPressed))
        self.navigationItem.leftBarButtonItem = backButton
        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "LoginBackground".backgroundImageName())
        self.tableView.backgroundView = imgvw
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.setupTableHeaderView()
        self.setupTableViewCells()
        self.setUpFooterView()
        
    }

    //TABLE HEADER VIEW
    override func setupTableHeaderView (){
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: tableHeaderHeight))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let labelNameText : UILabel = UILabel()
        labelNameText.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        labelNameText.textColor = UIColor.white
        labelNameText.textAlignment = .center
        labelNameText.numberOfLines = 0
        labelNameText.text = "Forget your password? No problem! We are here for help you to recover you access account. \n\n Please, tell us your email address and we will send your password to your inbox.".localize
        viewHeader.addSubview(labelNameText)
        constrain(labelNameText,viewHeader) { (labelNameText,viewHeader) in
            labelNameText.leading == viewHeader.leading + 30
            labelNameText.trailing == viewHeader.trailing - 30
            labelNameText.bottom == viewHeader.bottom - 20
            labelNameText.height == 130
        }
        
        let topPadding : CGFloat = UIDevice().screenType == .iPhone5 ? 20 : 30
        let imgLock = UIImageView(image: #imageLiteral(resourceName: "Lock"))
        viewHeader.addSubview(imgLock)
        constrain(imgLock,labelNameText,viewHeader) { (imgLock,labelNameText,viewHeader) in
            imgLock.bottom == labelNameText.top - topPadding
            imgLock.height == 40
            imgLock.width == 40
            imgLock.centerX == viewHeader.centerX
        }
        
    }
    
    //TABLE VIEW CELLS
    override func setupTableViewCells (){
        emailCell.setupWith(placeholder: "YOUR EMAIL".localize, keyBoardType: UIKeyboardType.default, isSecureText: false, leftImage: #imageLiteral(resourceName: "EmailCircle"), pickerType: .None)
        arrayTableViewCells.append(emailCell)
        self.tableView.reloadData()
    }
    
    //TABLE FOOTER VIEW
    override func setUpFooterView (){
        let height = UIDevice().screenHeight - (self.viewModel.loginTableCellHeight() * 2) - tableHeaderHeight - UIDevice().statusBarHeight
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: height))
        self.tableView.tableFooterView = viewFooter
        
        //RECOVER BUTTON
        let buttonSearch = UIButton(type: UIButtonType.custom)
        buttonSearch.configure(title: "PASSWORD RECOVER".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 14.0), backgroundColor: UIColor.clear)
        buttonSearch.setBackgroundImage(#imageLiteral(resourceName: "BlueBackground"), for: UIControlState.normal)
        buttonSearch.addTarget(self, action: #selector(onPasswordRecoverButtonPressed), for: UIControlEvents.touchUpInside)
        viewFooter.addSubview(buttonSearch)
        constrain(buttonSearch,viewFooter) { (buttonSearch,view) in
            buttonSearch.top == view.top + 20
            buttonSearch.leading == view.leading + 80
            buttonSearch.trailing == view.trailing - 80
            buttonSearch.height == self.viewModel.loginTableCellHeight() - 10
        }

        //VIEW LINE
        let viewLine : UIView = UIView()
        viewLine.backgroundColor = UIColor.white
        viewFooter.addSubview(viewLine)
        constrain(viewLine,buttonSearch,viewFooter) { (view1,view2,view3) in
            view1.leading == view3.leading + 120
            view1.trailing == view3.trailing - 120
            view1.height == 1
            view1.top == view2.bottom + 30
        }
        
        // DO NOT KNOW EMAIL ID
        let button1 = UIButton(type: UIButtonType.custom)
        button1.configure(title: "I don't know my email ".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 13.0), backgroundColor: UIColor.clear)
        button1.addTarget(self, action: #selector(onForgotEmailButtonPressed), for: UIControlEvents.touchUpInside)
        button1.setImage(#imageLiteral(resourceName: "arrow"), for: UIControlState.normal)
        button1.semanticContentAttribute = .forceRightToLeft
        viewFooter.addSubview(button1)
        constrain(button1,viewFooter,viewLine) { (button1,view,viewLine) in
            button1.top == viewLine.bottom + 30
            button1.leading == view.leading + 60
            button1.trailing == view.trailing - 60
            button1.height == 30
        }

        
    }
    
    //MARK:- BUTTON ACTIONS
    //PASSWORD RECOVER BUTTON ACTION
    @objc func onPasswordRecoverButtonPressed (){
        
    }
    
    //DONT KNOW EMAIL
    @objc func onForgotEmailButtonPressed (){
        
    }

}
