//
//  GuestUserViewModel.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class GuestUserViewModel: GuestUserViewModelInput {
    
    private let wireframe: GuestUserWireframe
    weak var view: GuestUserViewModelOutput!
    
    
    init(wireframe: GuestUserWireframe) {
        self.wireframe = wireframe
    }
    
    func loginTableCellHeight () -> CGFloat {
        switch UIDevice().screenType {
        case .iPhone6Plus,.iPhoneX:
            return 60
        default:
            return 50
        }
    }
    
    //SEARCH BUTTON
    func userPressedSearhButton (cpf : String, dob : String){
        if cpf.isEmpty{
            self.view.showAlert(with: "Digite seu cpf".localize)
        }else if dob.isEmpty{
            self.view.showAlert(with: "Por favor insira sua data de nascimento".localize)
        }else{
            SKActivityIndicator.show()
            let Params : [String : Any] = ["cpf" : cpf,
                                            "dob" : dob]
            self.searchWithJson(params: Params)
        }
    }
    
    func userPressedYesButton (){
        self.wireframe.didLogin()
    }
    
//    func userPressedNoButton (){
//        
//    }
    
    func userPressedGuestLoginButton (){
        self.wireframe.didLogin()
    }
    
    //SEARCH USER WITH JSON
    func searchWithJson (params : [String : Any]){
        ServiceManager.postDataFromService(baseurl: Constants.API.baseURL, serviceName: "recover_account", params: params, success: { (result) in
            SKActivityIndicator.dismiss()
            if let resultDict = result as? [String : Any],
                let status = resultDict["status"] as? NSNumber{
                if status == 0{
                    self.view.showAlert(with: "Detalhes inválidos! Por favor, tente novamente".localize)
                }else{
                    LoggedUser.saveLoggedUserdetails(dictDetails: resultDict["data"] as! NSDictionary)
                    LoggedUser.shared.storeLoggedUserDetails(dict: resultDict["data"] as! NSDictionary)
                    self.wireframe.gotoGuestUserPage3()
                }
            }
        }) { (error) in
            SKActivityIndicator.dismiss()
            self.view.showAlert(with: error.localizedDescription)
        }

    }

}
