//
//  GuestUserModule.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

struct GuestUserModule {
    
    static func build() -> GuestUserTableViewController {
        let wireframe = GuestUserWireframe()
        let viewModel = GuestUserViewModel(wireframe: wireframe)
        let viewController = GuestUserTableViewController(viewModel: viewModel)
        viewModel.view = viewController as GuestUserViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }
    
    static func buildPage1() -> GuestUser1TableViewController {
        let wireframe = GuestUserWireframe()
        let viewModel = GuestUserViewModel(wireframe: wireframe)
        let viewController = GuestUser1TableViewController(viewModel: viewModel)
        viewModel.view = viewController as GuestUserViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }

    static func buildPage2() -> GuestUser2TableViewController {
        let wireframe = GuestUserWireframe()
        let viewModel = GuestUserViewModel(wireframe: wireframe)
        let viewController = GuestUser2TableViewController(viewModel: viewModel)
        viewModel.view = viewController as GuestUserViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }
    
    static func buildPage3() -> GuestUser3TableViewController {
        let wireframe = GuestUserWireframe()
        let viewModel = GuestUserViewModel(wireframe: wireframe)
        let viewController = GuestUser3TableViewController(viewModel: viewModel)
        viewModel.view = viewController as GuestUserViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }


}
