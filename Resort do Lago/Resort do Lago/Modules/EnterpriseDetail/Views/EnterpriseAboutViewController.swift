//
//  EnterpriseAboutViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class EnterpriseAboutViewController: UIViewController {
    
    internal var companyData : Company!
    var tabHeight : CGFloat?
    internal var webViewAbout : UIWebView = UIWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.setupView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupView (){
        self.view.addSubview(webViewAbout)
        webViewAbout.backgroundColor = UIColor.white
        webViewAbout.loadHTMLString(companyData.companyAbout, baseURL: nil)
        constrain(webViewAbout,self.view) { (webViewAbout, view) in
            webViewAbout.top == view.top
            webViewAbout.leading == view.leading
            webViewAbout.trailing == view.trailing
            webViewAbout.bottom == view.bottom - tabHeight!
        }
    }
    
}
