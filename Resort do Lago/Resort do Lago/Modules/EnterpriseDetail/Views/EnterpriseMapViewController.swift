//
//  EnterpriseMapViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class EnterpriseMapViewController: UIViewController {

    internal var companyData : Company!
    var tabHeight : CGFloat?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        setupImageView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupImageView (){
        let imageviewMap = UIImageView()
        imageviewMap.contentMode = .scaleAspectFit
        self.view.addSubview(imageviewMap)
        constrain(imageviewMap,self.view) { (imageviewMap, view) in
            imageviewMap.top == view.top
            imageviewMap.leading == view.leading
            imageviewMap.trailing == view.trailing
            imageviewMap.bottom == view.bottom - tabHeight!
        }
        if let url = URL(string: companyData.companyMapImage){
            imageviewMap.kf.setImage(with: url)
        }

    }


}
