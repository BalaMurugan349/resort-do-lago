//
//  EnterpriseDetailViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class EnterpriseDetailViewController: AMPagerTabsViewController,EnterpriseDetailViewModelOutput {
    
    private let viewModel: EnterpriseDetailViewModelInput
    internal var companyData : Company!
    
    init(viewModel: EnterpriseDetailViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Grupo Resort do Lago"
        self.setupView()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- SETUP UI
    func setupView (){
        self.view.backgroundColor = UIColor.white
        
        //Label Title
        let labelTitle : UILabel = UILabel()
        self.view.addSubview(labelTitle)
        labelTitle.font = Font.make(FontName.varelaRound, weight: FontWeight.normal
            , size: 15.0)
        labelTitle.textColor = UIColor.navigationBackground
        labelTitle.backgroundColor = UIColor.labelBackground
        labelTitle.textAlignment = .center
        labelTitle.text = companyData.companyName
        constrain(labelTitle,self.view) { (labelTitle,view) in
            labelTitle.leading == view.leading
            labelTitle.trailing == view.trailing
            labelTitle.top == view.top
            labelTitle.height == 30
        }
        
        settings.tabBackgroundColor = UIColor.topTabBackground
        settings.tabButtonColor = UIColor.topTabBackground
        tabFont = UIFont.systemFont(ofSize: 15, weight: .semibold)
        settings.tabHeight = 50
        self.isPagerScrollEnabled = false
        self.viewControllers = getTabs()
        
    }
    
    func getTabs() -> [UIViewController]{
        // instantiate the viewControllers
        let aboutVC = EnterpriseAboutViewController(nibName: nil, bundle: nil)
        aboutVC.companyData = companyData
        aboutVC.title = "SOBRE"
        aboutVC.tabHeight = self.tabBarController?.tabBar.frame.height

        let galleryVC = EnterpriseGalleryViewController(nibName: nil, bundle: nil)
        galleryVC.companyData = companyData
        galleryVC.title = "GALERIA"
        galleryVC.tabHeight = self.tabBarController?.tabBar.frame.height

        let mapVC = EnterpriseMapViewController(nibName: nil, bundle: nil)
        mapVC.companyData = companyData
        mapVC.title = "MAPA"
        mapVC.tabHeight = self.tabBarController?.tabBar.frame.height

        let joinVC = EnterpriseJoinViewController(nibName: nil, bundle: nil)
        joinVC.companyData = companyData
        joinVC.title = "JUNTE-SE"
        
        return [aboutVC,galleryVC,mapVC,joinVC]
    }

    
}
