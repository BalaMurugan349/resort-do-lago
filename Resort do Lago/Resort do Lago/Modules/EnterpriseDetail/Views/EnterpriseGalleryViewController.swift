//
//  EnterpriseGalleryViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class EnterpriseGalleryViewController: UIViewController,UICollectionViewDelegateFlowLayout {
    
    internal var companyData : Company!
    private var collectioViewGallery : UICollectionView!
    var tabHeight : CGFloat?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.setupCollectionView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupCollectionView (){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let width = UIDevice().screenWidth/2
        layout.itemSize = CGSize(width: width , height: width)
        collectioViewGallery = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectioViewGallery.delegate = self
        collectioViewGallery.dataSource = self
        collectioViewGallery.register(GalleryCollectionViewCell.self, forCellWithReuseIdentifier: "GalleryCell")
        collectioViewGallery.backgroundColor = UIColor.white
        self.view.addSubview(collectioViewGallery)
        constrain(collectioViewGallery,self.view) { (collectioViewGallery, view) in
            collectioViewGallery.top == view.top
            collectioViewGallery.leading == view.leading
            collectioViewGallery.trailing == view.trailing
            collectioViewGallery.bottom == view.bottom - tabHeight!
        }

    }
}

extension EnterpriseGalleryViewController : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return companyData.companyGallery.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : GalleryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath as IndexPath) as! GalleryCollectionViewCell
        cell.setupCell(galleryData: companyData.companyGallery[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
