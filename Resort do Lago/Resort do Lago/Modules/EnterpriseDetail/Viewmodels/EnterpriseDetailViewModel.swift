//
//  EnterpriseDetailViewModel.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class EnterpriseDetailViewModel: EnterpriseDetailViewModelInput {
    
    private let wireframe: EnterpriseDetailWireframe
    weak var view: EnterpriseDetailViewModelOutput!
    
    init(wireframe: EnterpriseDetailWireframe) {
        self.wireframe = wireframe
    }
}
