//
//  EnterpriseDetailModule.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

struct EnterpriseDetailModule {
    
    static func build(data: Company) -> EnterpriseDetailViewController {
        let wireframe = EnterpriseDetailWireframe()
        let viewModel = EnterpriseDetailViewModel(wireframe: wireframe)
        let viewController = EnterpriseDetailViewController(viewModel: viewModel)
        viewController.companyData = data
        viewModel.view = viewController as EnterpriseDetailViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }
}
