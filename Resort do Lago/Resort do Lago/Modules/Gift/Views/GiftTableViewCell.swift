//
//  GiftTableViewCell.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/26/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class GiftTableViewCell: UITableViewCell {
    
    var textfieldCustom : UITextField = UITextField()
    var whatsappButton : UIButton = UIButton(type: UIButtonType.custom)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setupWith (placeholder : String ,keyboardType : UIKeyboardType){
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        textfieldCustom.placeholder = placeholder
        textfieldCustom.attributedPlaceholder = NSAttributedString(string:placeholder,
                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        textfieldCustom.textColor = UIColor.white
        textfieldCustom.keyboardType = keyboardType
        textfieldCustom.delegate = self
        textfieldCustom.font = Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0)
        self.contentView.addSubview(textfieldCustom)
        constrain(textfieldCustom,self.contentView) { (textfield,contentView) in
            textfield.top == contentView.top + 20
            textfield.leading == contentView.leading + 50
            textfield.trailing == contentView.trailing - 50
            textfield.bottom == contentView.bottom - 5
        }
        if keyboardType == .numberPad { self.setupInputAccessoryView() }
        
        let viewLine = UIView()
        viewLine.backgroundColor = UIColor.white
        self.contentView.addSubview(viewLine)
        constrain(viewLine,textfieldCustom) { (viewLine,textfield) in
            viewLine.top == textfield.bottom
            viewLine.leading == textfield.leading
            viewLine.trailing == textfield.trailing
            viewLine.height == 1
        }

    }
    
    func setupWhatsAppButton (){
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        self.selectionStyle = .none
        whatsappButton.configure(title: "  Esse número é o whatsapp", titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.clear)
        whatsappButton.addTarget(self, action: #selector(onWhatsappButtonPressed(sender:)), for: UIControlEvents.touchUpInside)
        whatsappButton.setImage(#imageLiteral(resourceName: "CheckboxUnSelected"), for: UIControlState.normal)
        whatsappButton.setImage(#imageLiteral(resourceName: "CheckboxSelected"), for: UIControlState.selected)
        whatsappButton.contentHorizontalAlignment = .left
        self.contentView.addSubview(whatsappButton)
        constrain(whatsappButton,self.contentView) { (whatsappButton,contentView) in
            whatsappButton.top == contentView.top + 20
            whatsappButton.leading == contentView.leading + 55
            whatsappButton.trailing == contentView.trailing - 55
            whatsappButton.bottom == contentView.bottom - 5
        }

    }
    
    //INPUT ACCESSORY VIEW - TOOLBAR
    func setupInputAccessoryView (){
        let toolBar = UIToolbar()
        let doneButton = UIBarButtonItem(title: "Done".localize, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onDoneButtonPressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.sizeToFit()
        textfieldCustom.inputAccessoryView = toolBar
    }

    //MARK:- BUTTON ACTIONS
    //DONE BUTTON
    @objc func onDoneButtonPressed (){
        textfieldCustom.resignFirstResponder()
    }
    
    @objc func onWhatsappButtonPressed (sender : UIButton){
        sender.isSelected = !sender.isSelected
    }

}

//MARK:- UITEXTFIELD DELEGATE
extension GiftTableViewCell : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
