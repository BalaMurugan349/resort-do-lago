//
//  GiftTableViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class GiftTableViewController: UITableViewController,GiftViewModelOutput {

    private let viewModel: GiftViewModelInput
    private var arrayTableviewCells : [GiftTableViewCell] = []
    private let nameCell : GiftTableViewCell = GiftTableViewCell()
    private let contactNumberCell : GiftTableViewCell = GiftTableViewCell()
    private let whatsappCell : GiftTableViewCell = GiftTableViewCell()
    private let emailCell : GiftTableViewCell = GiftTableViewCell()

    init(viewModel: GiftViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        self.navigationItem.title = "Grupo Resort do Lago"
        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "GiftBg".backgroundImageName())
        self.tableView.backgroundView = imgvw
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.setUpTableHeaderView()
        self.setupTableViewCells()
        self.setupTableFooterView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //TABLE HEADER VIEW
    func setUpTableHeaderView (){
        let headerHeight : CGFloat = UIDevice().screenType == .iPhone5 ? 170 : 150
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: headerHeight))
        viewHeader.backgroundColor = UIColor.clear
        
        let labelHeading = UILabel()
        viewHeader.addSubview(labelHeading)
        labelHeading.font = Font.make(FontName.varelaRound, weight: FontWeight.normal
            , size: 15.0)
        labelHeading.textColor = UIColor.navigationBackground
        labelHeading.backgroundColor = UIColor.labelBackground
        labelHeading.textAlignment = .center
        labelHeading.text = "Um presente muito especial para você!"
        constrain(labelHeading,viewHeader) { (labelHeading,viewHeader) in
            labelHeading.leading == viewHeader.leading
            labelHeading.trailing == viewHeader.trailing
            labelHeading.top == viewHeader.top
            labelHeading.height == 30
        }
        
        let imageviewIcon = UIImageView()
        imageviewIcon.image = #imageLiteral(resourceName: "GiftIcon")
        imageviewIcon.contentMode = .scaleAspectFit
        viewHeader.addSubview(imageviewIcon)
        constrain(imageviewIcon,labelHeading) { (imageviewIcon,labelHeading) in
            imageviewIcon.leading == labelHeading.leading + 15
            imageviewIcon.width == 50
            imageviewIcon.top == labelHeading.bottom + 15
            imageviewIcon.height == 50
        }

        let labelDescription = UITextView()
        viewHeader.addSubview(labelDescription)
        labelDescription.font = Font.make(FontName.varelaRound, weight: FontWeight.normal
            , size: 13.0)
        labelDescription.textColor = UIColor.white
        labelDescription.backgroundColor = UIColor.clear
        labelDescription.isUserInteractionEnabled = false
        labelDescription.text = "Nossa equipe de vendas preparou para você um presente especial de boas-vindas. Tenha a oportunidade de conhecer uma de nossas empresas. Basta preencher o formulário de contato e nosso representante entrará em contato com você!"
        constrain(labelDescription,imageviewIcon,viewHeader) { (labelDescription,imageviewIcon,viewHeader) in
            labelDescription.leading == imageviewIcon.trailing + 5
            labelDescription.trailing == viewHeader.trailing - 5
            labelDescription.top == imageviewIcon.top - 5
            labelDescription.bottom == viewHeader.bottom
        }

        self.tableView.tableHeaderView = viewHeader
    }
    
    func setupTableViewCells (){
        nameCell.setupWith(placeholder: "Seu nome completo", keyboardType: UIKeyboardType.default)
        arrayTableviewCells.append(nameCell)
        
        contactNumberCell.setupWith(placeholder: "Seu telefone de contato", keyboardType: UIKeyboardType.numberPad)
        arrayTableviewCells.append(contactNumberCell)

        whatsappCell.setupWhatsAppButton()
        arrayTableviewCells.append(whatsappCell)

        emailCell.setupWith(placeholder: "E-MAIL", keyboardType: UIKeyboardType.default)
        arrayTableviewCells.append(emailCell)
        self.tableView.reloadData()
    }

    //TABLE FOOTER VIEW
    func setupTableFooterView (){
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: 90))
        viewFooter.backgroundColor = UIColor.clear
        
        //REQUEST MY GIFT BUTTON
        let buttonRequest = UIButton(type: UIButtonType.custom)
        buttonRequest.configure(title: "Solicite meu presente", titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.navigationBackground)
        buttonRequest.addTarget(self, action: #selector(onRequestMyGiftButtonPressed), for: UIControlEvents.touchUpInside)
        buttonRequest.layer.cornerRadius = 8.0
        buttonRequest.clipsToBounds = true
        viewFooter.addSubview(buttonRequest)
        constrain(buttonRequest,viewFooter) { (buttonRequest,view) in
            buttonRequest.top == view.top + 20
            buttonRequest.leading == view.leading + 80
            buttonRequest.trailing == view.trailing - 80
            buttonRequest.height == 50
        }
        self.tableView.tableFooterView = viewFooter

    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTableviewCells.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.viewModel.giftTableCellHeight()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return arrayTableviewCells[indexPath.row]
    }
    
    //MARK:- BUTTON ACTION
    @objc func onRequestMyGiftButtonPressed (){
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    override var prefersStatusBarHidden: Bool{
        return false
    }

}
