//
//  GiftModule.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

struct GiftModule {
    
    static func build() -> GiftTableViewController {
        let wireframe = GiftWireframe()
        let viewModel = GiftViewModel(wireframe: wireframe)
        let viewController = GiftTableViewController(viewModel: viewModel)
        viewModel.view = viewController as GiftViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }
}
