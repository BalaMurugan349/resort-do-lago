//
//  GiftViewModel.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class GiftViewModel: GiftViewModelInput {
    
    private let wireframe: GiftWireframe
    weak var view: GiftViewModelOutput!
    
    init(wireframe: GiftWireframe) {
        self.wireframe = wireframe
    }
    
    func giftTableCellHeight () -> CGFloat {
        switch UIDevice().screenType {
        case .iPhone6Plus,.iPhoneX:
            return 75
        default:
            return 65
        }
    }

}
