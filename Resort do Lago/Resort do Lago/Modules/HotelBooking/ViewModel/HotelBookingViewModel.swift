//
//  HotelBookingViewModel.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class HotelBookingViewModel: HotelBookingViewModelInput {
    
    private let wireframe: HotelBookingWireframe
    weak var view: HotelBookingViewModelOutput!
    
    init(wireframe: HotelBookingWireframe) {
        self.wireframe = wireframe
    }
}
