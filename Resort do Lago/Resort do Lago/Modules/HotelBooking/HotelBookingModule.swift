//
//  HotelBookingModule.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/25/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

struct HotelBookingModule {
    
    static func build() -> HotelBookingViewController {
        let wireframe = HotelBookingWireframe()
        let viewModel = HotelBookingViewModel(wireframe: wireframe)
        let viewController = HotelBookingViewController(viewModel: viewModel)
        viewModel.view = viewController as HotelBookingViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }
}
