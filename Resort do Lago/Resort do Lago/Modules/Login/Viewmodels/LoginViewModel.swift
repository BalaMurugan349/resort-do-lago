//
//  LoginViewModel.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class LoginViewModel: LoginViewModelInput {
    
    private let wireframe: LoginWireframe
    weak var view: LoginViewModelOutput!

    init(wireframe: LoginWireframe) {
        self.wireframe = wireframe
    }

    func loginTableCellHeight () -> CGFloat {
        switch UIDevice().screenType {
        case .iPhone6Plus,.iPhoneX:
            return 60
        default:
            return 50
        }
    }
    
    //LOGIN
    func userPressedLoginButton (email : String, password : String){
        if email.isEmpty || !email.isValidEmail{
            self.view.showAlert(with: "Digite o e-mail válido".localize)
        }else if password.isEmpty{
            self.view.showAlert(with: "Por favor insira a senha".localize)
        }else{
            SKActivityIndicator.show()
            let loginParams : [String : Any] = ["email" : email,
                                                "password" : password]
            self.loginWithApi(params: loginParams)
        }
    }
    
    //DO NOT KNOW RESORT ID
    func userPressedForgotResortID (){
        wireframe.gotoGuestUserPage()
    }
    
    //FORGOT PASSWORD
    func userPressedForgotPassword()  {
        wireframe.gotoForgotPasswordPage()
    }
    
    //LOGIN WITH API
    func loginWithApi(params : [String : Any])  {
        ServiceManager.postDataFromService(baseurl: Constants.API.baseURL, serviceName: "login", params: params, success: { (result) in
            SKActivityIndicator.dismiss()
            if let resultDict = result as? [String : Any],
                let status = resultDict["status"] as? NSNumber{
                if status == 0{
                    self.view.showAlert(with: "Falha na autenticação! Por favor, tente novamente".localize)
                }else{
                    //LOGIN SUCCESS
                    LoggedUser.saveLoggedUserdetails(dictDetails: resultDict["data"] as! NSDictionary)
                    LoggedUser.shared.storeLoggedUserDetails(dict: resultDict["data"] as! NSDictionary)
                    self.wireframe.didLogin()
                }
            }
        }) { (error) in
            SKActivityIndicator.dismiss()
            self.view.showAlert(with: error.localizedDescription)
        }
    }


}
