//
//  LoginInterfaces.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewModelOutput: class {
    func showAlert(with message: String)
}

protocol LoginViewModelInput: class {
    func loginTableCellHeight () -> CGFloat
    func userPressedLoginButton (email : String, password : String)
    func userPressedForgotResortID ()
    func userPressedForgotPassword()
}
