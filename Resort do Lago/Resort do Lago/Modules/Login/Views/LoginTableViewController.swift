//
//  LoginTableViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class LoginTableViewController: UITableViewController,LoginViewModelOutput {

    private let viewModel: LoginViewModelInput
    private let tableHeaderHeight : CGFloat = UIDevice().screenHeight * 0.30
    private var arrayTableViewCells : [LoginTableViewCell] = []
    private let emailCell : LoginTableViewCell = LoginTableViewCell()
    private let passwordCell : LoginTableViewCell = LoginTableViewCell()

    init(viewModel: LoginViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }

    
    //MARK:- SETUP UI
    func setupView (){
        self.title = "Associado"
        self.navigationItem.hidesBackButton = true
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "BackButton"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(onBackButtonPressed))
        self.navigationItem.leftBarButtonItem = backButton
        
        let helpButton = UIBarButtonItem(title: "Ajuda", style: UIBarButtonItemStyle.plain, target: self, action: #selector(onHelpButtonPressed))
        self.navigationItem.rightBarButtonItem = helpButton

        let imgvw = UIImageView(frame: self.view.bounds)
        imgvw.image = UIImage(named: "LoginBackground".backgroundImageName())
        self.tableView.backgroundView = imgvw
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.setupTableHeaderView()
        self.setupTableViewCells()
        self.setUpFooterView()

    }
    
    //TABLE HEADER VIEW
    func setupTableHeaderView (){
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: tableHeaderHeight))
        viewHeader.backgroundColor = UIColor.clear
        self.tableView.tableHeaderView = viewHeader
        
        let imgvwLogo = UIImageView(image: #imageLiteral(resourceName: "ResortLogo"))
        viewHeader.addSubview(imgvwLogo)
        constrain(imgvwLogo,viewHeader) { (imgvwLogo,viewHeader) in
            imgvwLogo.width == 190
            imgvwLogo.height == 125
            imgvwLogo.centerX == viewHeader.centerX
            imgvwLogo.centerY == viewHeader.centerY - 20
        }
        
        let imgvwresortId = UIImageView(image: #imageLiteral(resourceName: "ResortId"))
        viewHeader.addSubview(imgvwresortId)
        constrain(imgvwresortId,imgvwLogo,viewHeader) { (imgvwresortId,imgvwLogo,viewHeader) in
            imgvwresortId.width == 230
            imgvwresortId.height == 60
            imgvwresortId.centerX == viewHeader.centerX
            imgvwresortId.top == imgvwLogo.bottom  - 20
        }

    }

    //TABLE VIEW CELLS
    func setupTableViewCells (){
        emailCell.setupWith(placeholder: "E-MAIL".localize, keyBoardType: UIKeyboardType.emailAddress, isSecureText: false, leftImage: #imageLiteral(resourceName: "Email"), pickerType: .None)
        arrayTableViewCells.append(emailCell)
        
        passwordCell.setupWith(placeholder: "SENHA".localize, keyBoardType: UIKeyboardType.default, isSecureText: true, leftImage: #imageLiteral(resourceName: "Password"), pickerType: .None)
        arrayTableViewCells.append(passwordCell)
        self.tableView.reloadData()
    }

    //TABLE FOOTER VIEW
    func setUpFooterView (){
        let height = UIDevice().screenHeight - (self.viewModel.loginTableCellHeight() * 2) - tableHeaderHeight - UIDevice().navigationBarHeight
        let viewFooter = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: height))
        self.tableView.tableFooterView = viewFooter
        
        //VIEW LINE
        let viewLine : UIView = UIView()
        viewLine.backgroundColor = UIColor.white
        viewFooter.addSubview(viewLine)
        constrain(viewLine,viewFooter) { (view1,view2) in
            view1.leading == view2.leading + 120
            view1.trailing == view2.trailing - 120
            view1.height == 1
            view1.top == view2.top + 15
        }

        //LOGIN BUTTON
        let buttonLogin = UIButton(type: UIButtonType.custom)
        buttonLogin.configure(title: "ENTRAR".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 15.0), backgroundColor: UIColor.clear)
        buttonLogin.setBackgroundImage(#imageLiteral(resourceName: "BlueBackground"), for: UIControlState.normal)
        buttonLogin.addTarget(self, action: #selector(onLoginButtonPressed), for: UIControlEvents.touchUpInside)
        viewFooter.addSubview(buttonLogin)
        constrain(buttonLogin,viewFooter,viewLine) { (buttonLogin,view,viewLine) in
            buttonLogin.top == viewLine.top + 20
            buttonLogin.leading == view.leading + 80
            buttonLogin.trailing == view.trailing - 80
            buttonLogin.height == self.viewModel.loginTableCellHeight() - 10
        }
        
        // BUTTON
        let button1 = UIButton(type: UIButtonType.custom)
        button1.configure(title: "Não sei meu ResortID ".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 13.0), backgroundColor: UIColor.clear)
        button1.addTarget(self, action: #selector(onForgotResortidPressed), for: UIControlEvents.touchUpInside)
        button1.setImage(#imageLiteral(resourceName: "arrow"), for: UIControlState.normal)
        button1.semanticContentAttribute = .forceRightToLeft
        viewFooter.addSubview(button1)
        constrain(button1,viewFooter,buttonLogin) { (button1,view,buttonLogin) in
            button1.top == buttonLogin.bottom + 30
            button1.leading == view.leading + 60
            button1.trailing == view.trailing - 60
            button1.height == 30
        }

        // BUTTON
        let button2 = UIButton(type: UIButtonType.custom)
        button2.configure(title: "Esqueci minha Senha ".localize, titleColor: UIColor.white, font: Font.make(FontName.varelaRound, weight: FontWeight.normal, size: 13.0), backgroundColor: UIColor.clear)
        button2.addTarget(self, action: #selector(onForgotPasswordPressed), for: UIControlEvents.touchUpInside)
        button2.setImage(#imageLiteral(resourceName: "key"), for: UIControlState.normal)
        button2.semanticContentAttribute = .forceRightToLeft
        viewFooter.addSubview(button2)
        constrain(button2,viewFooter,button1) { (button2,view,button1) in
            button2.top == button1.bottom
            button2.leading == view.leading + 60
            button2.trailing == view.trailing - 60
            button2.height == 30
        }


    }

    //MARK:- UITABLEVIEW DELEGATE
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTableViewCells.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.viewModel.loginTableCellHeight()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return arrayTableViewCells[indexPath.row]
    }
    
    //MARK:- BUTTON ACTIONS
    //LOGIN BUTTON
    @objc func onLoginButtonPressed (){
        self.viewModel.userPressedLoginButton(email: emailCell.textfieldCustom.text!, password: passwordCell.textfieldCustom.text!)
    }
    
    @objc func onForgotResortidPressed (){
        self.viewModel.userPressedForgotResortID()
    }
    
    @objc func onForgotPasswordPressed (){
        self.viewModel.userPressedForgotPassword()
    }
    
    //BACK BUTTON
    @objc func onBackButtonPressed (){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onHelpButtonPressed (){
        
    }
    
}
