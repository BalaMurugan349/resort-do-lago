//
//  LoginWireframe.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/16/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation
import UIKit

class LoginWireframe {
    
    weak var viewController: LoginTableViewController!

    //HOME
    func didLogin (){
        (UIApplication.shared.delegate as! AppDelegate).didLogin()
    }
    
    func gotoGuestUserPage (){
        let guestVC = GuestUserModule.build()
        viewController.navigationController?.pushViewController(guestVC, animated: true)
    }

    func gotoForgotPasswordPage() {
        let guestVC = GuestUserModule.buildPage2()
        viewController.navigationController?.pushViewController(guestVC, animated: true)

    }

}
