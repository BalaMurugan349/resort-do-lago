//
//  HomeModule.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

struct HomeModule {
    
    static func build() -> HomeViewController {
        let wireframe = HomeWireframe()
        let viewModel = HomeViewModel(wireframe: wireframe)
        let viewController = HomeViewController(viewModel: viewModel)
        viewModel.view = viewController as HomeViewModelOutput
        wireframe.viewController = viewController
        
        return viewController
    }
}
