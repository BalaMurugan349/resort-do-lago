//
//  HomeTableViewCell.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 3/24/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Kingfisher
import Cartography

class HomeTableViewCell: UITableViewCell {

    var imageviewCompany : UIImageView = UIImageView()
    var labelCompanyName : UILabel = UILabel()
    var textviewCompanyDescription : UITextView = UITextView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(companyData : Company){
        self.backgroundColor = UIColor.cellBackground
        self.contentView.backgroundColor = UIColor.cellBackground
        self.selectionStyle = .none
        
        //IMAGEVIEW
        self.addSubview(imageviewCompany)
        constrain(imageviewCompany,self) { (imageviewCompany, cell) in
            imageviewCompany.centerY == cell.centerY
            imageviewCompany.leading == cell.leading + 10
            imageviewCompany.width == 100
            imageviewCompany.height == 80
        }
        if let url = URL(string: Constants.API.imageBaseURL + companyData.companyImageName){
            imageviewCompany.kf.setImage(with: url)
        }
        
        //COMPANY NAME
        self.addSubview(labelCompanyName)
        constrain(labelCompanyName,imageviewCompany,self) { (labelCompanyName,imageviewCompany, cell) in
            labelCompanyName.leading == imageviewCompany.trailing + 10
            labelCompanyName.top == cell.top
            labelCompanyName.height == 30
            labelCompanyName.trailing == cell.trailing - 40
        }
        labelCompanyName.font = Font.make(FontName.varelaRound, weight: FontWeight.normal
            , size: 15.0)
        labelCompanyName.textColor = UIColor.navigationBackground
        labelCompanyName.text = companyData.companyName

        //COMPANY DESCRIPTION
        self.addSubview(textviewCompanyDescription)
        constrain(textviewCompanyDescription,labelCompanyName,self) { (textviewCompanyDescription,labelCompanyName, cell) in
            textviewCompanyDescription.leading == labelCompanyName.leading - 5
            textviewCompanyDescription.top == labelCompanyName.bottom - 10
            textviewCompanyDescription.bottom == cell.bottom - 5
            textviewCompanyDescription.trailing == cell.trailing - 40
        }
        textviewCompanyDescription.font = Font.make(FontName.varelaRound, weight: FontWeight.normal
            , size: 14.0)
        textviewCompanyDescription.textColor = UIColor.black
        textviewCompanyDescription.text = companyData.companyDescription
        textviewCompanyDescription.backgroundColor = .clear
        textviewCompanyDescription.isUserInteractionEnabled = false

    }

}
