//
//  HomeViewController.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import UIKit
import Cartography

class HomeViewController: UITableViewController,HomeViewModelOutput {
    
    private let viewModel: HomeViewModelInput
    private var enterpriseData = Enterprises()
    private var player : JVYoutubePlayerView = JVYoutubePlayerView()
    
    lazy var headerHeight: CGFloat = {
        return UIDevice().screenHeight * 0.30
    }()
    
    
    init(viewModel: HomeViewModelInput) {
        self.viewModel = viewModel
        super.init(style: UITableViewStyle.plain)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Grupo Resort do Lago"
        SKActivityIndicator.show()
        self.viewModel.getEnterprises()
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = UIRefreshControl()
            self.tableView.refreshControl?.addTarget(self, action: #selector(reloadData), for: UIControlEvents.valueChanged)
            self.tableView.refreshControl?.tintColor = UIColor.lightGray
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.beginReceivingRemoteControlEvents()
        self.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.player.stopVideo()
        UIApplication.shared.endReceivingRemoteControlEvents()
        self.resignFirstResponder()
    }
    
    //SETUP VIEW
    func setupView (){
        self.view.backgroundColor = UIColor.white
        self.tableView.separatorStyle = .none
        self.tableView.showsVerticalScrollIndicator = false
        self.initializeYoutubePlayer()
    }
    
    func initializeYoutubePlayer (){
        self.player.delegate = self
        self.player.autoplay = true;
        self.player.modestbranding = true;
        self.player.allowLandscapeMode = false;
        self.player.forceBackToPortraitMode = true;
        self.player.allowAutoResizingPlayerFrame = false;
        self.player.playsinline = true;
        self.player.controls = false
    }
    
    //HOME OUTPUT DELEGATES
    func updateUIWithEnterpriseData(enterprise: Enterprises) {
        self.enterpriseData = enterprise
        self.refreshControl?.endRefreshing()
        self.setUpTableHeaderView()
        self.tableView.reloadData()
    }
    
    func didFailedToFetchEnterpriseData() {
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl?.endRefreshing()
        }
    }
    
    //TABLE HEADER VIEW
    func setUpTableHeaderView (){
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: UIDevice().screenWidth, height: headerHeight + 30))
        viewHeader.backgroundColor = UIColor.white
        
        let labelHeading = UILabel()
        viewHeader.addSubview(labelHeading)
        labelHeading.font = Font.make(FontName.varelaRound, weight: FontWeight.normal
            , size: 15.0)
        labelHeading.textColor = UIColor.navigationBackground
        labelHeading.backgroundColor = UIColor.labelBackground
        labelHeading.textAlignment = .center
        labelHeading.text = "Conheça nossas empresas e ofertas"
        constrain(labelHeading,viewHeader) { (labelHeading,viewHeader) in
            labelHeading.leading == viewHeader.leading
            labelHeading.trailing == viewHeader.trailing
            labelHeading.top == viewHeader.top
            labelHeading.height == 30
        }
        
        viewHeader.addSubview(self.player)
        constrain(self.player,labelHeading,viewHeader) { (player,labelHeading,viewHeader) in
            player.leading == viewHeader.leading
            player.trailing == viewHeader.trailing
            player.top == labelHeading.bottom
            player.bottom == viewHeader.bottom
        }
        self.player.loadPlayer(withVideoURL: enterpriseData.video.videoUrl)
        self.tableView.tableHeaderView = viewHeader
        
    }
    
    //TABLE VIEW DELEGATES
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterpriseData.companies.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableviewCell")
        if cell == nil{
            cell = HomeTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "HomeTableviewCell")
        }
        (cell as! HomeTableViewCell).setup(companyData: enterpriseData.companies[indexPath.row])
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.gotoDetailPage(data: enterpriseData.companies[indexPath.row])
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }

    override var prefersStatusBarHidden: Bool{
        return false
    }
    
    //PULL TO REFRESH
    @objc func reloadData (){
        self.viewModel.getEnterprises()
    }
}

extension HomeViewController : JVYoutubePlayerDelegate{
    func playerView(_ playerView: JVYoutubePlayerView!, didChangeTo quality: JVPlaybackQuality) {
        self.player.setPlaybackQuality(kJVPlaybackQualityHD720)
    }
}
