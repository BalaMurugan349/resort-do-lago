//
//  HomeViewModel.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

class HomeViewModel: HomeViewModelInput {
    
    private let wireframe: HomeWireframe
    weak var view: HomeViewModelOutput!
    
    
    init(wireframe: HomeWireframe) {
        self.wireframe = wireframe
    }
    
    //GO TO DETAIL PAGE
    func gotoDetailPage (data : Company){
        self.wireframe.loadDetailPage(data: data)
    }
    
    //FETCH ENTERPRISES LIST FROM API
    func getEnterprises (){
        ServiceManager.getDataFromService(baseurl: Constants.API.baseURL, serviceName: "enterprises", params: nil, success: { (result) in
            SKActivityIndicator.dismiss()
            if let resultDict = result as? [String : Any],
                let status = resultDict["status"] as? NSNumber{
                if status == 0{
                    self.view.didFailedToFetchEnterpriseData()
                    self.view.showAlert(with: "Falha ao obter empresas e ofertas".localize)
                }else{
                    let enterprise  = Enterprises(JSON: resultDict)
                    self.view.updateUIWithEnterpriseData(enterprise: enterprise!)
                }
            }
        }) { (error) in
            SKActivityIndicator.dismiss()
            self.view.didFailedToFetchEnterpriseData()
            self.view.showAlert(with: error.localizedDescription)
        }
    }
}
