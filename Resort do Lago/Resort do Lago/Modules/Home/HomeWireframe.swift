//
//  HomeWireframe.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

class HomeWireframe {
    
    weak var viewController: HomeViewController!
    
    func loadDetailPage (data : Company){
        let detailVC = EnterpriseDetailModule.build(data: data)
        viewController.navigationController?.pushViewController(detailVC, animated: true)
    }
}
