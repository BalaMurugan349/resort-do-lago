//
//  HomeInterfaces.swift
//  Resort do Lago
//
//  Created by Bala Murugan on 2/17/18.
//  Copyright © 2018 Bala Murugan. All rights reserved.
//

import Foundation

protocol HomeViewModelOutput: class {
    func showAlert(with message: String)
    func updateUIWithEnterpriseData (enterprise : Enterprises)
    func didFailedToFetchEnterpriseData ()

}

protocol HomeViewModelInput: class {
    func getEnterprises ()
    func gotoDetailPage (data : Company)
}
